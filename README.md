talosctl image
==============

[Docker](https://www.docker.com/) image with talosctl command line tool to administrate a [Talos](https://www.talos.dev/) installation

Docker pull command::

    docker pull registry.esss.lu.se/ics-docker/talos:latest
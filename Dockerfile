FROM alpine:3.18.4

LABEL maintainer "anders.harrisson@ess.eu"

ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

RUN adduser -u 1000 -D talos users

# Install talosctl
ENV TALOSCTL_VERSION v1.5.4
RUN wget -q -O /usr/local/bin/talosctl https://github.com/siderolabs/talos/releases/download/${TALOSCTL_VERSION}/talosctl-$(uname -s | tr "[:upper:]" "[:lower:]")-amd64 \
  && chmod +x /usr/local/bin/talosctl

ENTRYPOINT ["/usr/local/bin/talosctl"]

USER talos
